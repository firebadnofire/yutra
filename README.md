# yutra

This repo is meant to be an open archive of citra and yuzu. All binaries are as hosted by github.com prior to Nintendo's takedown.

Rights are NOT reserved to me, Rights are reserved by the Yuzu/Citra team.

---

## Citra downloads:

Citra version: nightly build 2104

Windows: <a href=/firebadnofire/yutra/src/branch/main/citra/windows>Download</a><br>
Mac: <a href=/firebadnofire/yutra/src/branch/main/citra/mac>Download</a><br>
Linux: <a href=/firebadnofire/yutra/src/branch/main/citra/linux>Download</a><br>
Android: <a href=/firebadnofire/yutra/src/branch/main/citra/android>Download</a><br>
unified-source: <a href=/firebadnofire/yutra/src/branch/main/citra/unified-source>Download</a><br>
git-sources: <a href=/firebadnofire/yutra/src/branch/main/citra/git-sources>Download</a>

---

## Yuzu

Yuzu version: mainline-0-1731

Windows: <a href=/yutra/src/branch/main/yuzu/windows>Download</a><br>
Linux: <a href=/yutra/src/branch/main/yuzu/Linux>Download</a><br>
git-sources: <a href=/yutra/src/branch/main/yuzu/git-sources>Download</a><br>

---

## Repo:

Download all the saved emu builds and source code to mirror it somewhere else

Repo (tar.bz2): <a href=https://archuser.org/emus.tar.bz2>Download</a><br>
Repo (zip): <a href=https://archuser.org/emus.zip>Download</a><br>